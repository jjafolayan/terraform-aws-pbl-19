region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-013bf878966f1fce4"

ami-bastion = "ami-02c1d6f89744f2a77"

ami-nginx = "ami-0a16dc73bcf8627ba"

ami-sonar = "ami-048298133ac87acfa"

keypair = "pb"

master-password = "devopspblproject"

master-username = "jide"

account_no = "741625519474"

tags = {
  Enviroment      = "production"
  Owner-Email     = "DevOps-jideaffo@yahoo.com"
  Managed-By      = "Terraform"
  Billing-Account = "1234567890"
}